import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="py-monadic",
    version="0.4.0",
    author="Jason Brown",
    author_email="jasonbrown_dev@protonmail.com",
    description="Monads in python",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/jrbrown/py-monadic",
    project_urls={
        "Bug Tracker": "https://gitlab.com/jrbrown/py-monadic/-/issues",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU Lesser General Public License v2 (LGPLv2)",
        "Operating System :: POSIX"
    ],
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src", exclude=['tests']),
    python_requires=">=3.8",
)


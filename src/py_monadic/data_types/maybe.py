
from py_monadic.data_types.base import Monad, Constructor

from typing import TypeVar, Generic, Union, Callable, Optional
from functools import total_ordering

A = TypeVar('A')
B = TypeVar('B')


@total_ordering
class NothingC(Constructor):
    def __init__(self):
        pass

    def __str__(self):
        return f"Nothing"

    def __eq__(self, _):
        return True

    def __lt__(self, _):
        return False


@total_ordering
class JustC(Constructor, Generic[A]):
    def __init__(self, x: A):
        self.v = x

    def __str__(self):
        return f"Just {self.v}"

    def __eq__(self, y: 'JustC[A]'):
        return self.v == y.v

    def __lt__(self, y: 'JustC[A]'):
        return self.v == y.v


@total_ordering
class Maybe(Monad[A]):
    def __init__(self, x: Union[NothingC, JustC[A]]):
        self.val = x

    def __str__(self):
        return self.val.__str__()

    def _general_func(self, maybe_y: 'Maybe[A]',
                      if_same_func, if_different_func, args=None, kwargs=None):
        args = args if args is not None else []
        kwargs = kwargs if kwargs is not None else {}

        both_just = isinstance(self.val, JustC) and isinstance(maybe_y.val, JustC)
        both_nothing = isinstance(self.val, NothingC) and isinstance(maybe_y.val, NothingC)

        if both_just or both_nothing:
            return if_same_func(*args, **kwargs)
        else:
            return if_different_func(*args, **kwargs)


    def __eq__(self, maybe_y: 'Maybe[A]'):
        return self._general_func(maybe_y=maybe_y, args=[maybe_y.val],
                                  if_same_func=self.val.__eq__, if_different_func=lambda _: False)

    def __lt__(self, maybe_y: 'Maybe[A]'):
        return self._general_func(maybe_y=maybe_y, args=[maybe_y.val],
                                  if_same_func=self.val.__lt__, if_different_func=lambda _: False)

    def __fmap__(self, f: Callable[[A], B]) -> 'Maybe[B]':
        if isinstance(self.val, JustC):
            return Just(f(self.val.v))
        else:
            return Nothing()

    def __mjoin__(self: 'Maybe[Maybe[B]]') -> 'Maybe[B]':
        if isinstance(self.val, NothingC):
            return Nothing()
        elif not isinstance(self.val, JustC) or not isinstance(self.val.v, Maybe):
            raise Exception()
        else:
            if isinstance(self.val.v.val, JustC):
                return Just(self.val.v.val.v)
            else:
                return Nothing()

    @staticmethod
    def __mreturn__(x: A) -> 'Maybe[A]':
        return Just(x)


def get_maybe(self: Maybe[A]) -> Optional[A]:
    if isinstance(self.val, JustC):
        return self.val.v
    else:
        return None


def Nothing():
    return Maybe(NothingC())

def Just(x: A) -> Maybe[A]:
    return Maybe(JustC(x))


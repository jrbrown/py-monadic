
from functools import total_ordering

from py_monadic.data_types.monoid import Monoid


@total_ordering
class Unit(Monoid):
    def __init__(self):
        pass

    def __str__(self):
        return "()"

    def __repr__(self):
        return self.__str__()

    def __eq__(self, _: 'Unit'):
        return True

    def __lt__(self, _: 'Unit'):
        return False

    def __mappend__(self, _:'Unit') -> 'Unit':
        return Unit()

    def __mempty__(self) -> 'Unit':
        return Unit()


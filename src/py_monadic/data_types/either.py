
from functools import total_ordering
from typing import Generic, TypeVar, Callable, Union

from py_monadic.data_types.base import Monad, Constructor


A = TypeVar('A')
B = TypeVar('B')
C = TypeVar('C')


@total_ordering
class LeftC(Constructor, Generic[A]):
    def __init__(self, x: A):
        self.v = x

    def __str__(self):
        return f"Left {self.v}"

    def __eq__(self, y: 'LeftC[A]'):
        return self.v == y.v

    def __lt__(self, y: 'LeftC[A]'):
        return self.v == y.v


@total_ordering
class RightC(Constructor, Generic[A]):
    def __init__(self, x: A):
        self.v = x

    def __str__(self):
        return f"Right {self.v}"

    def __eq__(self, y: 'RightC[A]'):
        return self.v == y.v

    def __lt__(self, y: 'RightC[A]'):
        return self.v == y.v


@total_ordering
class Either(Monad[B], Generic[A,B]):
    def __init__(self, x: Union[LeftC[A], RightC[B]]):
        # Without type inference we need to store both so we can preserve the types properly
        self.x = x

    @property
    def is_right(self):
        return isinstance(self.x, RightC)

    def __str__(self):
        return self.x.__str__()

    def _general_func(self, either_y: 'Either[A, B]', both_left_func, both_right_func,
                      self_r_other_l_func, self_l_other_r_func, args=None, kwargs=None):
        args = args if args is not None else []
        kwargs = kwargs if kwargs is not None else {}

        both_left = (not self.is_right) and (not either_y.is_right)
        both_right = self.is_right and either_y.is_right

        if both_left:
            return both_left_func(*args, **kwargs)
        elif both_right:
            return both_right_func(*args, **kwargs)
        elif self.is_right:
            return self_r_other_l_func(*args, **kwargs)
        else:
            return self_l_other_r_func(*args, **kwargs)


    def __eq__(self, either_y: 'Either[A, B]'):
        return self._general_func(either_y=either_y, args=[either_y.x],
                                  both_left_func=lambda y: self.x.__eq__(y),
                                  both_right_func=lambda y: self.x.__eq__(y),
                                  self_l_other_r_func=lambda _: False,
                                  self_r_other_l_func=lambda _: False)

    def __lt__(self, either_y: 'Either[A, B]'):
        return self._general_func(either_y=either_y, args=[either_y.x],
                                  both_left_func=lambda y: self.x.__lt__(y),
                                  both_right_func=lambda y: self.x.__lt__(y),
                                  self_l_other_r_func=lambda _: True,
                                  self_r_other_l_func=lambda _: False)

    def __fmap__(self, f: Callable[[B], C]) -> 'Either[A, C]':
        return Either(RightC(f(self.x.v)) if self.is_right else self.x)

    def __mjoin__(self: 'Either[A, Either[A, B]]') -> 'Either[A, B]':
        if self.is_right:
            return self.x.v
        else:
            return self

    @staticmethod
    def __mreturn__(x: B) -> 'Either[A, B]':
        return Either(RightC(x))


def Left(x: A) -> Either[A, B]:
    return Either(LeftC(x))

def Right(x: B) -> Either[A, B]:
    return Either(RightC(x))


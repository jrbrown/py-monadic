
from abc import ABC, abstractmethod
from typing import TypeVar, Generic
from py_monadic.typesafe import TypeCheck

A = TypeVar('A')


class Monoid(ABC, Generic[A]):
    @abstractmethod
    def __mappend__(self, monoid_y: 'Monoid[A]') -> 'Monoid[A]':
        raise NotImplementedError

    @abstractmethod
    def __mempty__(self) -> 'Monoid[A]':
        raise NotImplementedError

@TypeCheck(Monoid, Monoid, return_type=Monoid)
def mappend(x: Monoid[A], y: Monoid[A]) -> Monoid[A]:
    return x.__mappend__(y)

@TypeCheck(Monoid, return_type=Monoid)
def mempty(example: Monoid[A]) -> Monoid[A]:
    return example.__mempty__()


class MStr(str, Monoid[str]):
    def __new__(cls, *args, **kwargs):
        return super().__new__(cls, *args, **kwargs)

    def __mappend__(self, str_y: 'MStr') -> 'MStr':
        return MStr(self + str_y)

    def __mempty__(self) -> 'MStr':
        return MStr("")


class MInt(int, Monoid[int]):
    def __new__(cls, *args, **kwargs):
        return super().__new__(cls, *args, **kwargs)

    def __mappend__(self, str_y: 'MInt') -> 'MInt':
        return MInt(self + str_y)

    def __mempty__(self) -> 'MInt':
        return MInt(0)


class MFloat(float, Monoid[float]):
    def __new__(cls, *args, **kwargs):
        return super().__new__(cls, *args, **kwargs)

    def __mappend__(self, str_y: 'MFloat') -> 'MFloat':
        return MFloat(self + str_y)

    def __mempty__(self) -> 'MFloat':
        return MFloat(0)



from py_monadic.data_types.base import Monad

from functools import total_ordering
from typing import TypeVar

A = TypeVar('A')


@total_ordering
class Identity(Monad[A]):
    def __init__(self, x: A):
        self.val = x

    def __str__(self):
        return f"Identity {self.val}"

    def __eq__(self, y: 'Identity[A]'):
        return self.val == y.val

    def __lt__(self, y: 'Identity[A]'):
        return self.val < y.val

    def __fmap__(self, f):
        return Identity(f(self.val))

    def __mjoin__(self):
        if not isinstance(self.val, Identity):
            raise Exception()
        else:
            return Identity(self.val.val)

    @staticmethod
    def __mreturn__(x):
        return Identity(x)


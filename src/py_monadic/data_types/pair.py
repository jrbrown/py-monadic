
from py_monadic.data_types.base import Monad
from py_monadic.data_types.monoid import Monoid, mappend, mempty
from py_monadic.data_types.list import List
from py_monadic.data_types.swapable import Swapable

from typing import TypeVar, Callable, Any
from functools import total_ordering

A = TypeVar('A')
B = TypeVar('B')
C = TypeVar('C')


@total_ordering
class Pair(Monad[A], Swapable[A, B], Monoid[A]):
    def __init__(self, x: Monoid[A], y: Monoid[B]):
        self.l = x
        self.r = y

    def __str__(self):
        return f"<{self.l.__str__()},{self.r.__str__()}>"

    def __eq__(self, pair_y: 'Pair[A,B]'):
        return self.l == pair_y.l and self.r == pair_y.r

    def __lt__(self, pair_y: 'Pair[A,B]'):
        if self.l == pair_y.l:
            return self.r < pair_y.r
        else:
            return self.l < pair_y.l

    def __fmap__(self, f: Callable[[Monoid[B]], Monoid[C]]) -> 'Pair[A,C]':
        return Pair(self.l, f(self.r))

    def __mjoin__(self: 'Pair[A,Pair[A, B]]') -> 'Pair[A,B]':
        return Pair(mappend(self.l, self.r.l), self.r.r)

    def __mreturn__(self, x: Monoid[A]) -> 'Pair[A, A]':
        return Pair(mempty(x),x)

    def __swap__(self: 'Pair[A, B]') -> 'Pair[B, A]':
        return Pair(self.r, self.l)

    def __mappend__(self: 'Pair[A, B]', x: 'Pair[A, B]') -> 'Pair[A, B]':
        return Pair(mappend(self.l, x.l), mappend(self.r, x.r))

    def __mempty__(self: 'Pair[A, B]') -> 'Pair[A, B]':
        return Pair(mempty(self.l), mempty(self.r))


def fst(p: Pair[A,Any]) -> Monoid[A]:
    return p.l

def snd(p: Pair[Any,B]) -> Monoid[B]:
    return p.r

def mzip(l1: List[Monoid[A]], l2: List[Monoid[B]]) -> List[Pair[A,B]]:
    return List([Pair(x,y) for x, y in zip(l1, l2)])



from py_monadic.data_types.base import Monad
from py_monadic.data_types.monoid import Monoid

from copy import copy
from typing import TypeVar, Callable

A = TypeVar('A')
B = TypeVar('B')


class List(list, Monad[A], Monoid[A]):
    def __init__(self, _list: list[A]):
        super().__init__(_list)

    def __str__(self):
        return f"List({super().__str__()})"

    def __fmap__(self: 'List[A]', f: Callable[[A], B]) -> 'List[B]':
        new_list = copy(self)
        return List(list(map(f, new_list)))

    def __mjoin__(self: 'List[List[A]]') -> 'List[A]':
        if self == []:
            return List([])
        elif not isinstance(self[0], list):
            print(self)
            raise Exception()
        else:
            return List([x for xs in self for x in xs])

    @staticmethod
    def __mreturn__(x: A) -> 'List[A]':
        return List([x])

    def __mappend__(self: 'List[A]', str_y: 'List[A]') -> 'List[A]':
        return List(self + str_y)

    def __mempty__(self) -> 'List':
        return List([])


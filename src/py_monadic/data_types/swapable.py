
from abc import ABC, abstractmethod
from typing import TypeVar, Generic
from py_monadic.typesafe import TypeCheck

A = TypeVar('A')
B = TypeVar('B')

class Swapable(ABC, Generic[A, B]):
    @abstractmethod
    def __swap__(self: 'Swapable[A,B]') -> 'Swapable[B,A]':
        raise NotImplementedError

@TypeCheck(Swapable, return_type=Swapable)
def swap(x: Swapable[A, B]) -> Swapable[B, A]:
    return x.__swap__()


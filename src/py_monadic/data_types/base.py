
from abc import ABC, abstractmethod
from typing import TypeVar, Generic

A = TypeVar('A')


class Functor(ABC, Generic[A]):
    def __repr__(self):
        return self.__str__()

    @abstractmethod
    def __str__(self):
        return super().__str__()

    @abstractmethod
    def __fmap__(self, _):
        raise NotImplementedError


class Monad(Functor[A]):
    @abstractmethod
    def __mjoin__(self):
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def __mreturn__(_):
        raise NotImplementedError


class Constructor:
    pass


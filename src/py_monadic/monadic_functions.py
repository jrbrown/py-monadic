
from typing import Callable, TypeVar

from py_monadic.typesafe import TypeCheck
from py_monadic.data_types.base import Functor, Monad


A = TypeVar('A')
B = TypeVar('B')
C = TypeVar('C')
D = TypeVar('D')


# NOTE TO READER
# The partially applicable decorator causes some issues with ide type checking
# Just ignore it

# Functor f => (a -> b) -> f a -> f b
@TypeCheck(Callable, Functor, return_type=Functor)
def fmap(func: Callable[[A], B], obj: Functor[A]) -> Functor[B]:
    return obj.__fmap__(func)

# Functor f => a -> f b -> f a
@TypeCheck(None, Functor, return_type=Functor)
def f_replace(new_val: A, f_obj: Functor) -> Functor[A]:
    return fmap(lambda _: new_val, f_obj)


# This function is mainly to help static type checkers not get confused
# Monad m => (a -> b) -> m a -> m b
@TypeCheck(Callable, Monad, return_type=Monad)
def mmap(func: Callable[[A], B], m: Monad[A]) -> Monad[B]:
    return m.__fmap__(func)

# Monad m => m (m a) -> m a
@TypeCheck(Monad, return_type=Monad)
def join(m_obj: Monad[Monad[A]]) -> Monad[A]:
    return m_obj.__mjoin__()

# Monad m => m a -> (a -> m b) -> m b
@TypeCheck(Monad, Callable, return_type=Monad)
def bind(m_obj: Monad[A], mf: Callable[[A], Monad[B]]) -> Monad[B]:
    return mmap(mf, m_obj).__mjoin__()

# Monad m => m a -> m b -> m b
@TypeCheck(Monad, Monad, return_type=Monad)
def m_replace(m_obj1: Monad, m_obj2: Monad[A]) -> Monad[A]:
    return bind(m_obj1, lambda _: m_obj2)

# Monad m => a -> m a
@TypeCheck(Monad, None, return_type=Monad)
def m_return(example: Monad, obj: A) -> Monad[A]:
    return example.__mreturn__(obj)

# Monad m => (a -> m b) -> (b -> m c) -> a -> m c
@TypeCheck(Callable, Callable, Callable, return_type=Monad)
def mf_compose(mf1: Callable[[A], Monad[B]], mf2: Callable[[B], Monad[C]], obj: A) -> Monad[C]:
    return bind(mf1(obj), mf2)


# liftP and liftP2 have applicative counterparts in Haskell
# We use monads to implement them to avoid excessive extra classes
# These can be implement more cleanly using partially applied functions but this confuses
# the type checker and we already have to be careful enough that they work properly

# Monad m => m (a -> b) -> m a -> m b
@TypeCheck(Monad, Monad, return_type=Monad)
def liftP(mf: Monad[Callable[[A], B]], m: Monad[A]) -> Monad[B]:
    return bind(mf, lambda f: mmap(f, m))

# Monad m => m (a -> b -> c) -> m a -> m b -> m c
@TypeCheck(Monad, Monad, Monad, return_type=Monad)
def liftP2(mf: Monad[Callable[[A, B], C]], m1: Monad[A], m2: Monad[B]) -> Monad[C]:
    return bind(mf, lambda f: bind(m1, lambda x: (bind(m2, lambda y: m_return(m1, f(x,y))))))

# Monad m => (a -> b -> c) -> m a -> m b -> m c
@TypeCheck(Callable, Monad, Monad, return_type=Monad)
def liftM2(f: Callable[[A, B], C], m1: Monad[A], m2: Monad[B]) -> Monad[C]:
    return bind(m1, lambda x: (bind(m2, lambda y: m_return(m1, f(x,y)))))



import unittest
from py_monadic.typesafe import GeneralCheck
from collections import OrderedDict
from typing import Union

from py_monadic.data_types.identity import Identity
from py_monadic.data_types.maybe import Just, Nothing
from py_monadic.data_types.either import Left, Right
from py_monadic.data_types.list import List
from py_monadic.data_types.base import Functor
from py_monadic.data_types.pair import Pair
from py_monadic.data_types.monoid import MInt

from py_monadic.monadic_functions import fmap


def test_fmap(x: Functor[int]):
    return fmap(lambda y: 2*y, x)


class FmapTests(unittest.TestCase):
    """Tests the fmap instances for each Functor

    Tests must carry out the operation correctly whilst also not mutating the input object
    """

    def testIdentity(self):
        x = Identity(3)
        self.assertEqual(test_fmap(x), Identity(6))
        self.assertEqual(x, Identity(3))

    def testMaybe(self):
        x = Just(5)
        self.assertEqual(test_fmap(x), Just(10))
        self.assertEqual(x, Just(5))
        y = Nothing()
        self.assertEqual(test_fmap(y), Nothing())

    def testList(self):
        x = List([1,2,3])
        self.assertEqual(test_fmap(x), List([2,4,6]))
        self.assertEqual(x, List([1,2,3]))

    def testPair(self):
        x = Pair(MInt(6), MInt(9))
        self.assertEqual(test_fmap(x), Pair(MInt(6), MInt(18)))
        self.assertEqual(x, Pair(MInt(6), MInt(9)))

    def testEither(self):
        x = Right(7)
        self.assertEqual(test_fmap(x), Right(14))
        self.assertEqual(x, Right(7))
        x = Left("Hello")
        self.assertEqual(test_fmap(x), Left("Hello"))


@GeneralCheck(arg_types=OrderedDict({"x": [int, float]}))
def safe_sqrt(x: Union[int, float]):
    return Nothing() if x < 0 else Just(x**0.5)

@GeneralCheck(arg_types=OrderedDict({"m": [int]}))
def mk_list_of_multiples(m, x):
    return List([x*(i+1) for i in range(m)])

@GeneralCheck(arg_properties=OrderedDict({"x": ["__neg__"]}))
def mk_pos_neg_pair(x):
    return List([x, -x])


if __name__ == "__main__":
    unittest.main()


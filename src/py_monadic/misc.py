from typing import Optional
from collections import defaultdict


def defaultNoneDict(d: Optional[dict]=None) -> defaultdict:
    x = defaultdict(lambda: None)
    if d is not None:
        x.update(d)
    return x


from typing import Callable, TypeVar

A = TypeVar('A')
B = TypeVar('B')
C = TypeVar('C')


def flip(func: Callable[[A, B], C]) -> Callable[[B, A], C]:
    def wrapper(x,y):
        return func(y,x)
    return wrapper

